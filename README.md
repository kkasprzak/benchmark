# Benchmark command

## Run project

#### Install Docker

[Docker website](https://www.docker.com/)

#### Install required packages and configuration
    
```bash
docker-compose run composer install
```
    
There are following configuration parameters:
    
```
mailer_host: <smtp server address>
mailer_port: <smtp server port>
mailer_user: <username> 
mailer_password: <password>
mailer_encryption: <ssl or empty>
email_from: <e-mail address from which notifications should be sent> 
email_to: <e-mail address to which notifications should be sent>
telephone: <telephone number to which sms notifications should be sent>
```

#### Benchmark loading time of the website in comparison to the other websites

```bash
docker-compose run app bin/console dummycorp:benchmark http://wp.pl http://onet.pl http://gazeta.pl
```

```
Usage:
  dummycorp:benchmark [options] [--] <website> <competitors> (<competitors>)...

Arguments:
  website               Your website to compare
  competitors           List of competitor websites to compare with

Options:
  -l, --log=LOG         File path where benchmark results should be logged to [default: "log.txt"]
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Help:
  This command benchmarks loading time of the website in comparison to the other
  websites. Checking how fast is the website's loading time in comparison
  to other competitors.
```

## Run tests

```bash

# Run phpspec
docker-compose run app vendor/bin/phpspec run

# Run phpspec for single file
docker-compose run app vendor/bin/phpspec run spec/DummyCorp/Benchmark/BenchmarkSpec.php

# Run phpspec for single method in file
docker-compose run app vendor/bin/phpspec run spec/DummyCorp/Benchmark/BenchmarkSpec.php:21

# Run phpunit
docker-compose run app vendor/bin/phpunit
```

## Run php code sniffer

```bash

# Run phpcs
docker-compose run app vendor/bin/phpcs -s

# Run phpcs for single file 
docker-compose run app vendor/bin/phpcs -s src/DummyCorp/Benchmark/Benchmark.php

# Run phpcbf for trying to fix some errors
docker-compose run app vendor/bin/phpcbf -s

# Run phpcbf for single file 
docker-compose run app vendor/bin/phpcbf -s src/DummyCorp/Benchmark/Benchmark.php

```

## Documentation

The benchmark consists of several basic elements:

#### Sampler

Responsible for measuring the response time of the server

```php
interface SamplerInterface
{
    /**
     * @param string $uri
     *
     * @return int
     */
    public function request(string $uri): int;
}
```

#### Benchmark

Coordinates the work of samplers

#### Report

Collects and keeps data from samplers

#### Writer

Exports data from the report to the indicated media

```php
interface WriterInterface
{
    /**
     * @param Report $report
     *
     * @return bool
     */
    public function export(Report $report): bool;
}
```

#### Email and Sms notification

Email and SMS notifications have been implemented based on the observer pattern to decople benchmark logic from notification.
There are two subscribers SendEmailSubscriber and SendSmsSubscriber which listens to new benchmark report event.