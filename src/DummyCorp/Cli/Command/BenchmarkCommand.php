<?php

declare(strict_types=1);

namespace DummyCorp\Cli\Command;

use DummyCorp\Benchmark\Benchmark;
use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Benchmark\Report;
use DummyCorp\Benchmark\Report\Writer\ConsoleWriter;
use DummyCorp\Benchmark\Report\Writer\FileWriter;
use DummyCorp\Benchmark\Sampler\HttpSampler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class BenchmarkCommand
 */
class BenchmarkCommand extends Command
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var Benchmark
     */
    private $benchmark;

    /**
     * BenchmarkCommand constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param string|null              $name
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, ?string $name = null)
    {
        parent::__construct($name);

        $this->eventDispatcher = $eventDispatcher;
        $this->benchmark = new Benchmark(new HttpSampler());
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->setName('dummycorp:benchmark')
            ->setDescription('Benchmark the site against competitors')
            ->setHelp(<<<EOD
This command benchmarks loading time of the website in comparison to the other
websites. Checking how fast is the website's loading time in comparison
to other competitors.
EOD);

        $this->addArgument(
            'website',
            InputArgument::REQUIRED,
            'Your website to compare'
        );
        $this->addArgument(
            'competitors',
            InputArgument::REQUIRED | InputArgument::IS_ARRAY,
            'List of competitor websites to compare with'
        );

        $this->addOption(
            'log',
            'l',
            InputOption::VALUE_REQUIRED,
            'File path where benchmark results should be logged to',
            'log.txt'
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $website = $input->getArgument('website');
        $competitors = $input->getArgument('competitors');
        $logFile = $input->getOption('log');

        $result = $this->benchmark->run(array_merge([$website], $competitors));

        $report = new Report($website, $result, new \DateTime());

        $this->eventDispatcher->dispatch(BenchmarkReportEvent::NAME, new BenchmarkReportEvent($report));

        $consoleWriter = new ConsoleWriter($output);
        $consoleWriter->export($report);

        $fileWriter = new FileWriter(new Filesystem(), $logFile);
        $fileWriter->export($report);
    }
}
