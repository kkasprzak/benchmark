<?php

declare(strict_types=1);

namespace DummyCorp\Notification;

/**
 * Class EmailGateway
 */
class EmailGateway
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $emailFrom;

    /**
     * EmailGateway constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param string        $emailFrom
     */
    public function __construct(\Swift_Mailer $mailer, string $emailFrom)
    {
        $this->mailer = $mailer;
        $this->emailFrom = $emailFrom;
    }

    /**
     * @param string $email
     * @param string $message
     *
     * @return bool
     */
    public function send(string $email, string $message): bool
    {
        $message = (new \Swift_Message('DummyCorp benchmark notification'))
            ->setFrom($this->emailFrom)
            ->setTo($email)
            ->setBody($message);

        return 0 !== $this->mailer->send($message);
    }
}
