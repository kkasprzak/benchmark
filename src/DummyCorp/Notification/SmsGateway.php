<?php

declare(strict_types=1);

namespace DummyCorp\Notification;

/**
 * Class SmsGateway
 */
class SmsGateway
{
    /**
     * @param string $number
     * @param string $message
     *
     * @return bool
     */
    public function send(string $number, string $message): bool
    {
        return true;
    }
}
