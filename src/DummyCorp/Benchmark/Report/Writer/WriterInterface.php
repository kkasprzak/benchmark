<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Report\Writer;

use DummyCorp\Benchmark\Report;

/**
 * Interface WriterInterface
 */
interface WriterInterface
{
    /**
     * @param Report $report
     *
     * @return bool
     */
    public function export(Report $report): bool;
}
