<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Report\Writer;

/**
 * Trait WriterTrait
 */
trait WriterTrait
{
    /**
     * @param int $siteExecutionTime
     * @param int $competitorExecutionTime
     *
     * @return string
     */
    private function getCompetitorComparison(
        int $siteExecutionTime,
        int $competitorExecutionTime
    ): string {
        $diff = $siteExecutionTime - $competitorExecutionTime;

        if ($diff > 0) {
            return abs($diff).' ms faster';
        }

        if ($diff < 0) {
            return abs($diff).' ms slower';
        }

        return 'same speed';
    }
}
