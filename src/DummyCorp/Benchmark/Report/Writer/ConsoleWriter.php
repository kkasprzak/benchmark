<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Report\Writer;

use DummyCorp\Benchmark\Report;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsoleWriter
 */
class ConsoleWriter implements WriterInterface
{
    use WriterTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * ConsoleWriter constructor.
     *
     * @param OutputInterface $output
     */
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function export(Report $report): bool
    {
        $this->output->writeln('Benchmark date: '.$report->getBenchmarkDate()->format('Y-m-d'));
        $this->output->writeln('Your website speed: '.$report->getBenchmarkSiteExecutionTime().' ms');
        $this->output->writeln('');

        $this->output->writeln('Competitor comparison');
        $this->output->writeln(' ---------------------------------------------------------------');
        $this->output->writeln('| Website                        | Speed [ms] | Comparison [ms] |');
        $this->output->writeln(' ---------------------------------------------------------------');

        $siteExecutionTime = $report->getBenchmarkSiteExecutionTime();

        foreach ($report->getBenchmarkCompetitorsExecutionTime() as $competitorSite => $competitorExecutionTime) {
            $comparison = $this->getCompetitorComparison(
                $siteExecutionTime,
                $competitorExecutionTime
            );

            $this->output->writeln(sprintf('| %-30s | %-10d | %-+15d |', mb_strimwidth($competitorSite, 0, 30), $competitorExecutionTime, $competitorExecutionTime - $siteExecutionTime));
        }

        $this->output->writeln(' ---------------------------------------------------------------');

        return true;
    }
}
