<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Report\Writer;

use DummyCorp\Benchmark\Report;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileWriter
 */
class FileWriter implements WriterInterface
{
    use WriterTrait;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $path;

    /**
     * FileWriter constructor.
     *
     * @param Filesystem $filesystem
     * @param string     $path
     */
    public function __construct(Filesystem $filesystem, string $path)
    {
        $this->filesystem = $filesystem;
        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function export(Report $report): bool
    {
        $content = <<<EOD
Benchmark date: {$report->getBenchmarkDate()->format('Y-m-d')}
Your website speed: {$report->getBenchmarkSiteExecutionTime()} ms

Competitor comparison
 ---------------------------------------------------------------
| Website                        | Speed [ms] | Comparison [ms] |
 ---------------------------------------------------------------
EOD;

        $siteExecutionTime = $report->getBenchmarkSiteExecutionTime();

        foreach ($report->getBenchmarkCompetitorsExecutionTime() as $competitorSite => $competitorExecutionTime) {
            $content .= "\n".sprintf('| %-30s | %-10d | %-+15d |', mb_strimwidth($competitorSite, 0, 30), $competitorExecutionTime, $competitorExecutionTime - $siteExecutionTime);
        }

        $content .= "\n ---------------------------------------------------------------";

        $this->filesystem->dumpFile($this->path, $content);

        return true;
    }
}
