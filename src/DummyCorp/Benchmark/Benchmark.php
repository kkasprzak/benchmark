<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark;

use DummyCorp\Benchmark\Sampler\SamplerInterface;

/**
 * Class Benchmark
 */
class Benchmark
{
    /**
     * @var SamplerInterface
     */
    private $sampler;

    /**
     * Benchmark constructor.
     *
     * @param SamplerInterface $sampler
     */
    public function __construct(SamplerInterface $sampler)
    {
        $this->sampler = $sampler;
    }

    /**
     * @param array $urls
     *
     * @return array
     */
    public function run(array $urls): array
    {
        $result = [];

        foreach ($urls as $url) {
            $result[$url] = $this->sampler->request($url);
        }

        return $result;
    }
}
