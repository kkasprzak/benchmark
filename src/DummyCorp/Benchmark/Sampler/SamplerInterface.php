<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Sampler;

/**
 * Interface SamplerInterface
 */
interface SamplerInterface
{
    /**
     * @param string $uri
     *
     * @return int
     */
    public function request(string $uri): int;
}
