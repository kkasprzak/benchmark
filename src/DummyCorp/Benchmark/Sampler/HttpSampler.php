<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Sampler;

/**
 * Class HttpSampler
 */
class HttpSampler implements SamplerInterface
{
    /**
     * CURL Object
     *
     * @var resource
     */
    protected $cURL;

    /**
     * HttpSampler constructor.
     */
    public function __construct()
    {
        $this->cURL = curl_init();
    }

    /**
     * HttpSampler destructor.
     */
    public function __destruct()
    {
        if (is_resource($this->cURL)) {
            curl_close($this->cURL);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function request(string $uri): int
    {
        curl_setopt($this->cURL, CURLOPT_URL, $uri);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER, 1);

        if (!$curl = curl_exec($this->cURL)) {
            $error = $this->getError();
            throw new \RuntimeException($error['error'], $error['error_no']);
        }

        return $this->getRequestTotalTime();
    }

    /**
     * @return array|null
     */
    private function getError(): ?array
    {
        if (curl_errno($this->cURL) > 0) {
            return [
                'error_no' => curl_errno($this->cURL),
                'error' => curl_error($this->cURL),
            ];
        }

        return null;
    }

    /**
     * @return int
     */
    private function getRequestTotalTime(): int
    {
        $info = curl_getinfo($this->cURL);

        return intval(round($info['total_time'] * 1000));
    }
}
