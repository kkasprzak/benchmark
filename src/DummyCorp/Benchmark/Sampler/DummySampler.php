<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Sampler;

/**
 * Class DummySampler
 */
class DummySampler implements SamplerInterface
{
    /**
     * {@inheritdoc}
     */
    public function request(string $uri): int
    {
        return random_int(1, 100);
    }
}
