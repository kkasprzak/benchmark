<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark;

/**
 * Class Report
 */
class Report
{
    /**
     * @var string
     */
    private $benchmarkSiteUrl;

    /**
     * @var array
     */
    private $benchmarkResult;

    /**
     * @var \DateTime
     */
    private $benchmarkDate;

    /**
     * Report constructor.
     *
     * @param string    $benchmarkSiteUrl
     * @param array     $benchmarkResult
     * @param \DateTime $benchmarkDate
     */
    public function __construct(string $benchmarkSiteUrl, array $benchmarkResult, \DateTime $benchmarkDate)
    {
        $this->benchmarkSiteUrl = $benchmarkSiteUrl;
        $this->benchmarkResult = $benchmarkResult;
        $this->benchmarkDate = $benchmarkDate;
    }

    /**
     * @return \DateTime
     */
    public function getBenchmarkDate(): \DateTime
    {
        return $this->benchmarkDate;
    }

    /**
     * @return int
     */
    public function getBenchmarkSiteExecutionTime(): int
    {
        return $this->benchmarkResult[$this->benchmarkSiteUrl];
    }

    /**
     * @return array
     */
    public function getBenchmarkCompetitorsExecutionTime(): array
    {
        $competitorsExeTime = $this->benchmarkResult;

        unset($competitorsExeTime[$this->benchmarkSiteUrl]);

        return $competitorsExeTime;
    }
}
