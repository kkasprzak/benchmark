<?php

declare(strict_types=1);

namespace DummyCorp\Benchmark\Event;

use DummyCorp\Benchmark\Report;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class BenchmarkReportEvent
 */
class BenchmarkReportEvent extends Event
{
    public const NAME = 'dummycorp.benchmark.event.report';

    /**
     * @var Report
     */
    private $report;

    /**
     * BenchmarkReportEvent constructor.
     *
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    /**
     * @return Report
     */
    public function getReport(): Report
    {
        return $this->report;
    }
}
