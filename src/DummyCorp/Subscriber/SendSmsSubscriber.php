<?php

declare(strict_types=1);

namespace DummyCorp\Subscriber;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Notification\SmsGateway;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SendSmsSubscriber
 */
class SendSmsSubscriber implements EventSubscriberInterface
{
    /**
     * @var SmsGateway
     */
    private $smsGateway;

    /**
     * @var string
     */
    private $telephoneNumber;

    /**
     * SendSmsSubscriber constructor.
     *
     * @param SmsGateway $smsGateway
     * @param string     $telephoneNumber
     */
    public function __construct(SmsGateway $smsGateway, string $telephoneNumber)
    {
        $this->smsGateway = $smsGateway;
        $this->telephoneNumber = $telephoneNumber;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            BenchmarkReportEvent::NAME => 'onBenchmarkReport',
        ];
    }

    /**
     * @param BenchmarkReportEvent $event
     */
    public function onBenchmarkReport(BenchmarkReportEvent $event)
    {
        $report = $event->getReport();
        $siteExecutionTime = $report->getBenchmarkSiteExecutionTime();

        foreach ($report->getBenchmarkCompetitorsExecutionTime() as $competitor => $competitorExecutionTime) {
            if ($siteExecutionTime / $competitorExecutionTime >= 2) {
                $this->smsGateway->send($this->telephoneNumber, 'Your website is twice slower than '.$competitor.' website!');
                break;
            }
        }
    }
}
