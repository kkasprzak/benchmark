<?php

declare(strict_types=1);

namespace DummyCorp\Subscriber;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Notification\EmailGateway;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SendEmailSubscriber
 */
class SendEmailSubscriber implements EventSubscriberInterface
{
    /**
     * @var EmailGateway
     */
    private $emailGateway;

    /**
     * @var string
     */
    private $emailTo;

    /**
     * SendEmailSubscriber constructor.
     *
     * @param EmailGateway $emailGateway
     * @param string       $emailTo
     */
    public function __construct(EmailGateway $emailGateway, string $emailTo)
    {
        $this->emailGateway = $emailGateway;
        $this->emailTo = $emailTo;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            BenchmarkReportEvent::NAME => 'onBenchmarkReport',
        ];
    }

    /**
     * @param BenchmarkReportEvent $event
     */
    public function onBenchmarkReport(BenchmarkReportEvent $event)
    {
        $report = $event->getReport();
        $siteExecutionTime = $report->getBenchmarkSiteExecutionTime();

        foreach ($report->getBenchmarkCompetitorsExecutionTime() as $competitor => $competitorExecutionTime) {
            if ($siteExecutionTime > $competitorExecutionTime) {
                $this->emailGateway->send($this->emailTo, 'Your website is slower than '.$competitor.' website!');
                break;
            }
        }
    }
}
