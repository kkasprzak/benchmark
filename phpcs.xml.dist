<?xml version="1.0"?>
<ruleset name="DummyCorp_Standard">
    <description>The DummyCorp coding standard</description>

    <file>spec</file>
    <file>src</file>
    <file>test</file>

    <arg name="basepath" value="."/>
    <arg name="colors" />
    <arg name="parallel" value="75" />
    <arg value="np" />

    <config name="installed_paths" value="vendor/slevomat/coding-standard"/>

    <!-- Don't hide tokenizer exceptions -->
    <rule ref="Internal.Tokenizer.Exception">
        <type>error</type>
    </rule>

    <!-- Include the whole Symfony standard -->
    <rule ref="vendor/escapestudios/symfony2-coding-standard/Symfony">
        <exclude name="Symfony.Functions.Arguments.Invalid" />
    </rule>

    <!-- Include some sniffs from other standards that don't conflict with Symfony -->
    <rule ref="Generic.Arrays.DisallowLongArraySyntax.Found" />
    <rule ref="Squiz.WhiteSpace.SemicolonSpacing.Incorrect" />
    <rule ref="SlevomatCodingStandard.TypeHints.DeclareStrictTypes">
        <properties>
            <property name="spacesCountAroundEqualsSign" value="0" />
            <property name="newlinesCountBetweenOpenTagAndDeclare" value="2" />
        </properties>
    </rule>

    <rule ref="Symfony.Commenting.FunctionComment.Missing">
        <exclude-pattern>spec/*</exclude-pattern>
        <exclude-pattern>tests/*</exclude-pattern>
    </rule>

    <rule ref="PSR1.Methods.CamelCapsMethodName.NotCamelCaps">
        <exclude-pattern>spec/*</exclude-pattern>
    </rule>
</ruleset>
