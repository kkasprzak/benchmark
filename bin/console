#!/usr/bin/env php
<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Yaml\Yaml;
use DummyCorp\Cli\Command\BenchmarkCommand;
use DummyCorp\Subscriber\SendEmailSubscriber;
use DummyCorp\Subscriber\SendSmsSubscriber;
use DummyCorp\Notification\EmailGateway;
use DummyCorp\Notification\SmsGateway;

$config = Yaml::parseFile(__DIR__.'/../parameters.yml')['parameters'];

$transport = (new \Swift_SmtpTransport($config['mailer_host'], $config['mailer_port'], $config['mailer_encryption']))
    ->setUsername($config['mailer_user'])
    ->setPassword($config['mailer_password'])
;

$mailer = new \Swift_Mailer($transport);

$eventDispatcher = new EventDispatcher();
$eventDispatcher->addSubscriber(new SendEmailSubscriber(new EmailGateway($mailer, $config['email_from']), $config['email_to']));
$eventDispatcher->addSubscriber(new SendSmsSubscriber(new SmsGateway(), $config['telephone']));

$application = new Application();
$application->add(new BenchmarkCommand($eventDispatcher));
$application->run();