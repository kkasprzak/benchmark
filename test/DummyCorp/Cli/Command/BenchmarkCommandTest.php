<?php

declare(strict_types=1);

namespace test\DummyCorp\Cli\Command;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Cli\Command\BenchmarkCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class BenchmarkCommandTest
 */
class BenchmarkCommandTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testCommandExecution()
    {
        $eventDispatcherMock = $this->getMockBuilder(EventDispatcher::class)
            ->setMethods(['dispatch'])
            ->getMock();

        $eventDispatcherMock->expects($this->once())
            ->method('dispatch')
            ->with($this->equalTo(BenchmarkReportEvent::NAME));

        $benchmarkCommand = new BenchmarkCommand($eventDispatcherMock);

        $commandTester = new CommandTester($benchmarkCommand);
        $commandTester->execute([
            'website' => 'http://wp.pl',
            'competitors' => ['http://onet.pl', 'http://gazeta.pl'],
            '--log' => '__phpunit-log-test.txt',
        ]);

        $this->assertFileExists('__phpunit-log-test.txt');
        $this->assertStringContainsString('http://onet.pl', $commandTester->getDisplay());

        unlink('__phpunit-log-test.txt');
    }
}
