<?php

declare(strict_types=1);

namespace spec\DummyCorp\Cli\Command;

use DummyCorp\Cli\Command\BenchmarkCommand;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class BenchmarkCommandSpec
 */
class BenchmarkCommandSpec extends ObjectBehavior
{
    public function let(EventDispatcherInterface $eventDispatcher)
    {
        $this->beConstructedWith($eventDispatcher);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(BenchmarkCommand::class);
    }

    public function it_should_be_an_instance_of_console_command()
    {
        $this->shouldBeAnInstanceOf(Command::class);
    }
}
