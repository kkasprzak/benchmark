<?php

declare(strict_types=1);

namespace spec\DummyCorp\Notification;

use DummyCorp\Notification\SmsGateway;
use PhpSpec\ObjectBehavior;

/**
 * Class SmsGatewaySpec
 */
class SmsGatewaySpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(SmsGateway::class);
    }

    public function it_sends_sms()
    {
        $this->send('+48123456789', 'Hey John Done. Your site is slow today!')->shouldBeBool();
    }
}
