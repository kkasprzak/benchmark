<?php

declare(strict_types=1);

namespace spec\DummyCorp\Notification;

use DummyCorp\Notification\EmailGateway;
use PhpSpec\ObjectBehavior;

/**
 * Class EmailGatewaySpec
 */
class EmailGatewaySpec extends ObjectBehavior
{
    public function let(\Swift_Mailer $mailer)
    {
        $this->beConstructedWith($mailer, 'johndoe@dummycorp.com');
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(EmailGateway::class);
    }

    public function it_sends_email()
    {
        $this->send('johndoe@dummycorp.com', 'Hey John Done. Your site is slow today!')->shouldBeBool();
    }
}
