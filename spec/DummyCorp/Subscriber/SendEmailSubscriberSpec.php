<?php

declare(strict_types=1);

namespace spec\DummyCorp\Subscriber;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Benchmark\Report;
use DummyCorp\Notification\EmailGateway;
use DummyCorp\Subscriber\SendEmailSubscriber;
use PhpSpec\ObjectBehavior;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SendEmailSubscriberSpec
 */
class SendEmailSubscriberSpec extends ObjectBehavior
{
    private const EMAIL = 'johndoe@dummycorp.com';

    public function let(EmailGateway $emailGateway)
    {
        $this->beConstructedWith($emailGateway, self::EMAIL);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(SendEmailSubscriber::class);
    }

    public function it_should_implement_event_subscriber_interface()
    {
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    public function it_should_return_array_of_subscribed_events()
    {
        $this->getSubscribedEvents()->shouldReturn([BenchmarkReportEvent::NAME => 'onBenchmarkReport']);
    }

    public function it_should_send_email_when_website_is_slower_than_at_least_one_competitor(
        Report $report,
        BenchmarkReportEvent $event,
        EmailGateway $emailGateway
    ) {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(100);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'url1' => 60,
            'url2' => 50,
            'url3' => 200,
        ]);

        $event->getReport()->willReturn($report);

        $emailGateway->send(self::EMAIL, 'Your website is slower than url1 website!')->shouldBeCalledOnce();

        $this->onBenchmarkReport($event)->shouldBeNull();
    }

    public function it_should_not_send_email_when_website_is_faster_than_all_competitors(
        Report $report,
        BenchmarkReportEvent $event,
        EmailGateway $emailGateway
    ) {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(100);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'url1' => 101,
            'url2' => 130,
            'url3' => 140,
        ]);

        $event->getReport()->willReturn($report);

        $emailGateway->send(self::EMAIL, 'Your website is slower than url1 website!')->shouldNotBeCalled();

        $this->onBenchmarkReport($event)->shouldBeNull();
    }
}
