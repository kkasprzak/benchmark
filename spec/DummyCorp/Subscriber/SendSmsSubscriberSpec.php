<?php

declare(strict_types=1);

namespace spec\DummyCorp\Subscriber;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Benchmark\Report;
use DummyCorp\Notification\SmsGateway;
use DummyCorp\Subscriber\SendSmsSubscriber;
use PhpSpec\ObjectBehavior;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SendSmsSubscriberSpec
 */
class SendSmsSubscriberSpec extends ObjectBehavior
{
    private const TELEPHONE = '+48606742290';

    public function let(SmsGateway $smsGateway)
    {
        $this->beConstructedWith($smsGateway, self::TELEPHONE);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(SendSmsSubscriber::class);
    }

    public function it_should_implement_event_subscriber_interface()
    {
        $this->shouldImplement(EventSubscriberInterface::class);
    }

    public function it_should_return_array_of_subscribed_events()
    {
        $this->getSubscribedEvents()->shouldReturn([BenchmarkReportEvent::NAME => 'onBenchmarkReport']);
    }

    public function it_should_send_sms_when_website_is_twice_slower_than_at_least_one_competitor(
        Report $report,
        BenchmarkReportEvent $event,
        SmsGateway $smsGateway
    ) {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(100);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'url1' => 60,
            'url2' => 50,
            'url3' => 200,
        ]);

        $event->getReport()->willReturn($report);

        $smsGateway->send(self::TELEPHONE, 'Your website is twice slower than url2 website!')->shouldBeCalledOnce();

        $this->onBenchmarkReport($event)->shouldBeNull();
    }

    public function it_should_not_send_sms_if_website_is_not_twice_slower_than_at_least_one_competitor(
        Report $report,
        BenchmarkReportEvent $event,
        SmsGateway $smsGateway
    ) {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(100);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'url1' => 51,
            'url2' => 70,
            'url3' => 200,
        ]);

        $event->getReport()->willReturn($report);

        $smsGateway->send()->shouldNotBeCalled();

        $this->onBenchmarkReport($event)->shouldBeNull();
    }
}
