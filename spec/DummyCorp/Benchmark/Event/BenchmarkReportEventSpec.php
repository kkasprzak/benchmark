<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark\Event;

use DummyCorp\Benchmark\Event\BenchmarkReportEvent;
use DummyCorp\Benchmark\Report;
use PhpSpec\ObjectBehavior;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class BenchmarkReportEventSpec
 */
class BenchmarkReportEventSpec extends ObjectBehavior
{
    public function let(Report $report)
    {
        $this->beConstructedWith($report);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(BenchmarkReportEvent::class);
    }

    public function it_should_have_event_type()
    {
        $this->shouldHaveType(Event::class);
    }

    public function it_should_return_report(Report $report)
    {
        $this->getReport()->shouldBeEqualTo($report);
    }
}
