<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark;

use DummyCorp\Benchmark\Sampler\SamplerInterface;
use DummyCorp\Benchmark\Benchmark;
use PhpSpec\ObjectBehavior;

/**
 * Class BenchmarkSpec
 */
class BenchmarkSpec extends ObjectBehavior
{
    public function let(SamplerInterface $sampler)
    {
        $this->beConstructedWith($sampler);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(Benchmark::class);
    }

    public function it_returns_loading_time_for_given_websites(SamplerInterface $sampler)
    {
        $sampler->request('url1')->willReturn(1);
        $sampler->request('url2')->willReturn(2);

        $result = $this->run(['url1', 'url2']);

        $result->shouldHaveKeyWithValue('url1', 1);
        $result->shouldHaveKeyWithValue('url2', 2);
    }
}
