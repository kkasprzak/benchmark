<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark\Sampler;

use DummyCorp\Benchmark\Sampler\DummySampler;
use DummyCorp\Benchmark\Sampler\SamplerInterface;
use PhpSpec\ObjectBehavior;

/**
 * Class DummySamplerSpec
 */
class DummySamplerSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(DummySampler::class);
    }

    public function it_should_implement_sampler_interface()
    {
        $this->shouldImplement(SamplerInterface::class);
    }
}
