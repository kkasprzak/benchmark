<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark\Sampler;

use DummyCorp\Benchmark\Sampler\HttpSampler;
use DummyCorp\Benchmark\Sampler\SamplerInterface;
use PhpSpec\ObjectBehavior;

/**
 * Class HttpSamplerSpec
 */
class HttpSamplerSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(HttpSampler::class);
    }

    public function it_should_implement_sampler_interface()
    {
        $this->shouldImplement(SamplerInterface::class);
    }

    public function it_return_request_total_time()
    {
        $this->request('http://onet.pl')->shouldBeInt();
    }

    public function it_throws_exception_when_error_occur()
    {
        $this->shouldThrow('\RuntimeException')->duringRequest('http://wrongadress09234.com');
    }
}
