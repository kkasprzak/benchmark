<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark\Report\Writer;

use DummyCorp\Benchmark\Report;
use DummyCorp\Benchmark\Report\Writer\FileWriter;
use DummyCorp\Benchmark\Report\Writer\WriterInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileWriterSpec
 */
class FileWriterSpec extends ObjectBehavior
{
    private const PATH = 'dummy_file.txt';

    public function let(Filesystem $filesystem)
    {
        $this->beConstructedWith($filesystem, self::PATH);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(FileWriter::class);
    }

    public function it_should_implement_writer_interface()
    {
        $this->shouldImplement(WriterInterface::class);
    }

    public function it_saves_report_to_file(Report $report, Filesystem $filesystem)
    {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(1000);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'http://www.gazeta.pl' => 45,
            'http://www.wp.pl' => 1230,
            'http://www.onet.pl' => 1000,
        ]);

        $expectedContent = <<<EOD
Benchmark date: 2019-03-02
Your website speed: 1000 ms

Competitor comparison
 ---------------------------------------------------------------
| Website                        | Speed [ms] | Comparison [ms] |
 ---------------------------------------------------------------
| http://www.gazeta.pl           | 45         | -955            |
| http://www.wp.pl               | 1230       | +230            |
| http://www.onet.pl             | 1000       | +0              |
 ---------------------------------------------------------------
EOD;

        $filesystem->dumpFile(self::PATH, $expectedContent)->shouldBeCalled();

        $this->export($report)->shouldBeBool();
    }
}
