<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark\Report\Writer;

use DummyCorp\Benchmark\Report;
use DummyCorp\Benchmark\Report\Writer\WriterInterface;
use DummyCorp\Benchmark\Report\Writer\ConsoleWriter;
use PhpSpec\ObjectBehavior;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsoleWriterSpec
 */
class ConsoleWriterSpec extends ObjectBehavior
{
    public function let(OutputInterface $output)
    {
        $this->beConstructedWith($output);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(ConsoleWriter::class);
    }

    public function it_should_implement_writer_interface()
    {
        $this->shouldImplement(WriterInterface::class);
    }

    public function it_export_report(Report $report, OutputInterface $output)
    {
        $report->getBenchmarkDate()->willReturn(new \DateTime('2019-03-02'));
        $report->getBenchmarkSiteExecutionTime()->willReturn(1000);
        $report->getBenchmarkCompetitorsExecutionTime()->willReturn([
            'http://www.gazeta.pl' => 45,
            'http://www.wp.pl' => 1230,
            'http://www.onet.pl' => 1000,
        ]);

        $output->writeln('Benchmark date: 2019-03-02')->shouldBeCalled();
        $output->writeln('Your website speed: 1000 ms')->shouldBeCalled();
        $output->writeln('')->shouldBeCalled();

        $output->writeln('Competitor comparison')->shouldBeCalled();
        $output->writeln(' ---------------------------------------------------------------')->shouldBeCalled();
        $output->writeln('| Website                        | Speed [ms] | Comparison [ms] |')->shouldBeCalled();
        $output->writeln(' ---------------------------------------------------------------')->shouldBeCalled();
        $output->writeln('| http://www.gazeta.pl           | 45         | -955            |')->shouldBeCalled();
        $output->writeln('| http://www.wp.pl               | 1230       | +230            |')->shouldBeCalled();
        $output->writeln('| http://www.onet.pl             | 1000       | +0              |')->shouldBeCalled();
        $output->writeln(' ---------------------------------------------------------------')->shouldBeCalled();

        $this->export($report)->shouldBeBool();
    }
}
