<?php

declare(strict_types=1);

namespace spec\DummyCorp\Benchmark;

use DummyCorp\Benchmark\Report;
use PhpSpec\ObjectBehavior;

/**
 * Class ReportSpec
 */
class ReportSpec extends ObjectBehavior
{
    public function let()
    {
        $this->beConstructedWith(
            'url1',
            ['url1' => 1, 'url2' => 2, 'url3' => 13, 'url4' => 21],
            new \DateTime('2019-03-01')
        );
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(Report::class);
    }

    public function it_returns_benchmark_date()
    {
        $this->getBenchmarkDate()->shouldBeLike(new \DateTime('2019-03-01'));
    }

    public function it_returns_execution_time_of_benchmarked_site()
    {
        $this->getBenchmarkSiteExecutionTime()->shouldBeEqualTo(1);
    }

    public function it_returns_execution_time_of_competitors()
    {
        $this->getBenchmarkCompetitorsExecutionTime()->shouldBeEqualTo(
            ['url2' => 2, 'url3' => 13, 'url4' => 21]
        );
    }
}
